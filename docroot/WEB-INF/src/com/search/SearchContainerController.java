package com.search;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SearchContainerController
 */
public class SearchContainerController extends MVCPortlet {
	
	private Log log = LogFactoryUtil.getLog(SearchContainerController.class);
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String tabName = ParamUtil.getString(renderRequest, "tabs", "UserGroup");
		
		/**
		 *  Depending on the parameter tabName, it will create searchcontainer 
		 */
		if (tabName.equals("UserGroup")) {
			UserGroupUtil.searchContainerData(renderRequest, renderResponse);
		} else if (tabName.equals("User")) {
			UserUtil.searchContainerData(renderRequest, renderResponse);
		}
		
		this.include(viewTemplate, renderRequest, renderResponse);
	}
	
	public void deleteUsers(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		//String[] parameterValues = ParamUtil.getParameterValues(actionRequest, "rowIds");
		String[] parameterValues = ParamUtil.getParameterValues(actionRequest, "idUser");
		
		for (String deleteOrganizationId : parameterValues) {
			log.info("idUser--" + deleteOrganizationId);
		}
		
		String parameterValues2 = ParamUtil.getString(actionRequest, "userAllRowIds");
		log.info("userAllRowIds--" + parameterValues2);
		
		long[] deleteOrganizationIds = StringUtil.split(ParamUtil.getString(actionRequest, "rowIds"), 0L);
		
		for (long deleteOrganizationId : deleteOrganizationIds) {
			log.info(deleteOrganizationId);
		}
	}
	
	//	public void processAction(ActionMapping actionMapping, ActionForm actionForm, PortletConfig portletConfig, ActionRequest actionRequest,
	//			ActionResponse actionResponse) throws Exception {
	//		
	//		try {
	//			String csv = getUsersCSV(actionRequest, actionResponse);
	//			
	//			String fileName = "users.csv";
	//			byte[] bytes = csv.getBytes();
	//			
	//			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
	//			HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
	//			
	//			ServletResponseUtil.sendFile(request, response, fileName, bytes, ContentTypes.TEXT_CSV_UTF8);
	//			
	//			setForward(actionRequest, ActionConstants.COMMON_NULL);
	//		} catch (Exception e) {
	//			SessionErrors.add(actionRequest, e.getClass());
	//			
	//			setForward(actionRequest, "portlet.users_admin.error");
	//		}
	//	}
	//	
	//	protected String getUserCSV(User user) {
	//		StringBundler sb = new StringBundler(PropsValues.USERS_EXPORT_CSV_FIELDS.length * 2);
	//		
	//		for (int i = 0; i < PropsValues.USERS_EXPORT_CSV_FIELDS.length; i++) {
	//			String field = PropsValues.USERS_EXPORT_CSV_FIELDS[i];
	//			
	//			if (field.equals("fullName")) {
	//				sb.append(CSVUtil.encode(user.getFullName()));
	//			} else if (field.startsWith("expando:")) {
	//				String attributeName = field.substring(8);
	//				
	//				ExpandoBridge expandoBridge = user.getExpandoBridge();
	//				
	//				sb.append(CSVUtil.encode(expandoBridge.getAttribute(attributeName)));
	//			} else {
	//				sb.append(CSVUtil.encode(BeanPropertiesUtil.getString(user, field)));
	//			}
	//			
	//			if ((i + 1) < PropsValues.USERS_EXPORT_CSV_FIELDS.length) {
	//				sb.append(StringPool.COMMA);
	//			}
	//		}
	//		sb.append(StringPool.NEW_LINE);
	//		return sb.toString();
	//	}
	//	
	//	protected List<User> getUsers(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
	//		
	//		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	//		
	//		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
	//		
	//		boolean exportAllUsers = PortalPermissionUtil.contains(permissionChecker, ActionKeys.EXPORT_USER);
	//		
	//		if (!exportAllUsers && !PortletPermissionUtil.contains(permissionChecker, PortletKeys.USERS_ADMIN, ActionKeys.EXPORT_USER)) {
	//			
	//			return Collections.emptyList();
	//		}
	//		
	//		PortletURL portletURL = ((ActionResponseImpl) actionResponse).createRenderURL(PortletKeys.USERS_ADMIN);
	//		
	//		UserSearch userSearch = new UserSearch(actionRequest, portletURL);
	//		
	//		UserSearchTerms searchTerms = (UserSearchTerms) userSearch.getSearchTerms();
	//		
	//		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	//		
	//		long organizationId = searchTerms.getOrganizationId();
	//		
	//		if (organizationId > 0) {
	//			params.put("usersOrgs", new Long(organizationId));
	//		} else if (!exportAllUsers) {
	//			User user = themeDisplay.getUser();
	//			
	//			Long[] organizationIds = ArrayUtil.toArray(user.getOrganizationIds(true));
	//			
	//			if (organizationIds.length > 0) {
	//				params.put("usersOrgs", organizationIds);
	//			}
	//		}
	//		
	//		long roleId = searchTerms.getRoleId();
	//		
	//		if (roleId > 0) {
	//			params.put("usersRoles", new Long(roleId));
	//		}
	//		
	//		long userGroupId = searchTerms.getUserGroupId();
	//		
	//		if (userGroupId > 0) {
	//			params.put("usersUserGroups", new Long(userGroupId));
	//		}
	//		
	//		if (PropsValues.USERS_INDEXER_ENABLED && PropsValues.USERS_SEARCH_WITH_INDEX) {
	//			
	//			params.put("expandoAttributes", searchTerms.getKeywords());
	//			
	//			Hits hits = null;
	//			
	//			if (searchTerms.isAdvancedSearch()) {
	//				hits = UserLocalServiceUtil.search(themeDisplay.getCompanyId(), searchTerms.getFirstName(), searchTerms.getMiddleName(),
	//						searchTerms.getLastName(), searchTerms.getScreenName(), searchTerms.getEmailAddress(), searchTerms.getStatus(), params,
	//						searchTerms.isAndOperator(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, (Sort) null);
	//			} else {
	//				hits = UserLocalServiceUtil.search(themeDisplay.getCompanyId(), searchTerms.getKeywords(), searchTerms.getStatus(), params,
	//						QueryUtil.ALL_POS, QueryUtil.ALL_POS, (Sort) null);
	//			}
	//			
	//			Tuple tuple = UsersAdminUtil.getUsers(hits);
	//			
	//			return (List<User>) tuple.getObject(0);
	//		}
	//		
	//		if (searchTerms.isAdvancedSearch()) {
	//			return UserLocalServiceUtil.search(themeDisplay.getCompanyId(), searchTerms.getFirstName(), searchTerms.getMiddleName(),
	//					searchTerms.getLastName(), searchTerms.getScreenName(), searchTerms.getEmailAddress(), searchTerms.getStatus(), params,
	//					searchTerms.isAndOperator(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, (OrderByComparator) null);
	//		} else {
	//			return UserLocalServiceUtil.search(themeDisplay.getCompanyId(), searchTerms.getKeywords(), searchTerms.getStatus(), params,
	//					QueryUtil.ALL_POS, QueryUtil.ALL_POS, (OrderByComparator) null);
	//		}
	//	}
	
}
